package pedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type RegistroCabecalho struct {
	IdentificadorTipoDeRegistro1    string `json:"IdentificadorTipoDeRegistro1"`
	SequencialDoArquivo             int64  `json:"SequencialDoArquivo"`
	IdentificadorDoTipoDeArquivoPed string `json:"IdentificadorDoTipoDeArquivoPed"`
	CodigoDoFornecedor              string `json:"CodigoDoFornecedor"`
	CodigoDoCliente                 string `json:"CodigoDoCliente"`
	NumeroDoPedido                  int64  `json:"NumeroDoPedido"`
	CodigoPromocao                  string `json:"CodigoPromocao"`
	NumeroCgcDoCLiente              string `json:"NumeroCgcDoCLiente"`
	TipoPedido                      string `json:"TipoPedido"`
}

func (r *RegistroCabecalho) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistroCabecalho

	err = posicaoParaValor.ReturnByType(&r.IdentificadorTipoDeRegistro1, "IdentificadorTipoDeRegistro1")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.SequencialDoArquivo, "SequencialDoArquivo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.IdentificadorDoTipoDeArquivoPed, "IdentificadorDoTipoDeArquivoPed")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CodigoDoFornecedor, "CodigoDoFornecedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CodigoDoCliente, "CodigoDoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.NumeroDoPedido, "NumeroDoPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CodigoPromocao, "CodigoPromocao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.NumeroCgcDoCLiente, "NumeroCgcDoCLiente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.TipoPedido, "TipoPedido")
	if err != nil {
		return err
	}

	return err
}

var PosicoesRegistroCabecalho = map[string]gerador_layouts_posicoes.Posicao{
	"IdentificadorTipoDeRegistro1":    {0, 1, 0},
	"SequencialDoArquivo":             {1, 6, 0},
	"IdentificadorDoTipoDeArquivoPed": {6, 9, 0},
	"CodigoDoFornecedor":              {9, 18, 0},
	"CodigoDoCliente":                 {18, 24, 0},
	"NumeroDoPedido":                  {24, 34, 0},
	"CodigoPromocao":                  {34, 44, 0},
	"NumeroCgcDoCLiente":              {44, 58, 0},
	"TipoPedido":                      {58, 59, 0},
}
