package pedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type RegistroDetalhe struct {
	IdentificadorTIpoDeRegistro2 string 	`json:"IdentificadorTIpoDeRegistro2"`
	SequencialDoArquivo          int64  	`json:"SequencialDoArquivo"`
	CodigoDoProduto              string 	`json:"CodigoDoProduto"`
	QuantidadePedida             int64  	`json:"QuantidadePedida"`
	CodigoEanDoProduto           string 	`json:"CodigoEanDoProduto"`
	PrecoUnitario                float64	`json:"PrecoUnitario"`
	PercentualDeDesconto         float64	`json:"PercentualDeDesconto"`
	PercentualDeRepasseIcms      float64	`json:"PercentualDeRepasseIcms"`
}

func (r *RegistroDetalhe) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistroDetalhe

	err = posicaoParaValor.ReturnByType(&r.IdentificadorTIpoDeRegistro2, "IdentificadorTIpoDeRegistro2")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.SequencialDoArquivo, "SequencialDoArquivo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CodigoDoProduto, "CodigoDoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.QuantidadePedida, "QuantidadePedida")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CodigoEanDoProduto, "CodigoEanDoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.PrecoUnitario, "PrecoUnitario")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.PercentualDeDesconto, "PercentualDeDesconto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.PercentualDeRepasseIcms, "PercentualDeRepasseIcms")
	if err != nil {
		return err
	}

	return err
}

var PosicoesRegistroDetalhe = map[string]gerador_layouts_posicoes.Posicao{
	"IdentificadorTIpoDeRegistro2":                      {0, 1, 0},
	"SequencialDoArquivo":                      {1, 6, 0},
	"CodigoDoProduto":                      {6, 14, 0},
	"QuantidadePedida":                      {14, 19, 0},
	"CodigoEanDoProduto":                      {19, 32, 0},
	"PrecoUnitario":                      {32, 41, 2},
	"PercentualDeDesconto":                      {41, 46, 2},
	"PercentualDeRepasseIcms":                      {46, 51, 2},
}