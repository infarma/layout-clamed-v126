package pedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type RegistroFinalizador struct {
	IdentificadorTIpoDeRegistro3     string	`json:"IdentificadorTIpoDeRegistro3"`
	SequencialDoArquivo              int64 	`json:"SequencialDoArquivo"`
	NumeroTotalDeProdutosPedidos     int64 	`json:"NumeroTotalDeProdutosPedidos"`
	QuantidadeTotalDeProdutosPedidos int64 	`json:"QuantidadeTotalDeProdutosPedidos"`
}

func (r *RegistroFinalizador) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistroFinalizador

	err = posicaoParaValor.ReturnByType(&r.IdentificadorTIpoDeRegistro3, "IdentificadorTIpoDeRegistro3")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.SequencialDoArquivo, "SequencialDoArquivo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.NumeroTotalDeProdutosPedidos, "NumeroTotalDeProdutosPedidos")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.QuantidadeTotalDeProdutosPedidos, "QuantidadeTotalDeProdutosPedidos")
	if err != nil {
		return err
	}

	return err
}

var PosicoesRegistroFinalizador = map[string]gerador_layouts_posicoes.Posicao{
	"IdentificadorTIpoDeRegistro3":                      {0, 1, 0},
	"SequencialDoArquivo":                      {1, 6, 0},
	"NumeroTotalDeProdutosPedidos":                      {6, 11, 0},
	"QuantidadeTotalDeProdutosPedidos":                      {11, 18, 0},
}