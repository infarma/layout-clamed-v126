Lista de Preços

    gerador_de_layouts listaDePrecos registroCabecalho IdentificadorDoTipoDeRegistro1:string:0:1 SequencialDoArquivo:int64:1:6 IdentificadorDoTipoPre:string:6:9 CodigoDoFornecedor:string:9:18 UnidadeDaFederacao:string:18:20 DataDaGeracaoDoArquivoDePreco:string:20:28
    
    gerador_de_layouts listaDePrecos registroDetalhe IdentificadorDoTipoDeRegistro2:string:0:1 SequencialDoArquivo:int64:1:6 CodigoDoProduto:string:6:14 PrecoUnitario:float64:14:23:2 PercentualDeDesconto:float64:23:28:2 PercentualDeRepasseDeIcms:float64:28:33:2 PercentualDeIcms:float64:33:38:2 PercentualDeReducaoDaBaseDeIcms:float64:38:43:2 PrecoBaseParaCalculoDoIcmsPorSubstituicaoTributaria:float64:43:52:2 ValorDoIcmsPorSubstituicaoTributaria:float64:52:61:2 NumeroDeDiasParaOPagamento:int64:61:64 CodigoEanDOProduto:string:64:84 ProdutoHpc:string:84:85 PercentualDeDescontoAdicionalDoPedidoEnviadoViaEntirePharmalink:float64:85:90:2 ValorDoIcmsPorSubstituicaoTributariaParaOsProdutosDaFarmacaiaPopularDoBrasil:float64:90:99:2 QuantidadeDeEstoqueDisponivel:int64:99:105 PercentualDeMidia:float64:105:110:2
    
    gerador_de_layouts listaDePrecos registroFinalizador IdentificadorDoTipoDeRegistro3:string:0:1 SequencialDoArquivo:int64:1:6 QuantidadeTotalDeRegistro:int64:6:11
    
Pedido

    gerador_de_layouts pedido registroCabecalho IdentificadorTipoDeRegistro1:string:0:1 SequencialDoArquivo:int64:1:6 IdentificadorDoTipoDeArquivoPed:string:6:9 CodigoDoFornecedor:string:9:18 CodigoDoCliente:string:18:24 NumeroDoPedido:int64:24:34 CodigoPromocao:string:34:44 NumeroCgcDoCLiente:string:44:58 TipoPedido:string:58:59

    gerador_de_layouts pedido registroDetalhe IdentificadorTIpoDeRegistro2:string:0:1 SequencialDoArquivo:int64:1:6 CodigoDoProduto:string:6:14 QuantidadePedida:int64:14:19 CodigoEanDoProduto:string:19:32 PrecoUnitario:float64:32:41:2 PercentualDeDesconto:float64:41:46:2 PercentualDeRepasseIcms:float64:46:51:2
    
    gerador_de_layouts pedido registroFinalizador IdentificadorTIpoDeRegistro3:string:0:1 SequencialDoArquivo:int64:1:6 NumeroTotalDeProdutosPedidos:int64:6:11 QuantidadeTotalDeProdutosPedidos:int64:11:18
    
Arquivo de Retorno

    gerador_de_layouts arquivoDeRetorno registroCabecalho IdentificadorTIpoDeRegistro1:string:0:1  SequencialDoArquivo:int64:1:6  IdentificadorDoTipoDeArquivoRet:string:6:9 CodigoDoFornecedor:string:9:18 CodigoDoCliente:string:18:24 NumeroDoPedido:int64:24:34 dentificadorDeRejeicaoDoPedido:string:34:35 DescricaoDoMotivoDaRejeicaoDoPedido:string:35:65 NumeroDoCgcDoCliente:string:65:79 TipoPedido:string:79:80 HorarioDeCorteParaOFaturamentoDoPedido:string:80:84

    gerador_de_layouts arquivoDeRetorno registroDetalhe IdentificadorTIpoDeRegistro2:string:0:1  SequencialDoArquivo:int64:1:6  codigoDoProduto:string:6:14 QuantidadeAtendida:int64:14:19 DescricaoDoMotivoDaRejeicao:string:19:69 CodigoEanDoProduto:string:69:82 OrigemDaMercadoria:string:82:83 
    
    gerador_de_layouts arquivoDeRetorno registroFinalizador IdentificadorDoRegistro3:string:0:1 SequencialDoArquivo:int64:1:6 NumeroTotalDeProdutosAtendidos:int64:6:11 NumeroTotaldeProdutosNaoAtendidos:int64:11:16                