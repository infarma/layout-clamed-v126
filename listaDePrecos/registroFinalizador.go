package listaDePrecos

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type RegistroFinalizador struct {
	IdentificadorDoTipoDeRegistro3 string `json:"IdentificadorDoTipoDeRegistro3"`
	SequencialDoArquivo            int64  `json:"SequencialDoArquivo"`
	QuantidadeTotalDeRegistro      int64  `json:"QuantidadeTotalDeRegistro"`
}

func (r *RegistroFinalizador) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistroFinalizador

	err = posicaoParaValor.ReturnByType(&r.IdentificadorDoTipoDeRegistro3, "IdentificadorDoTipoDeRegistro3")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.SequencialDoArquivo, "SequencialDoArquivo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.QuantidadeTotalDeRegistro, "QuantidadeTotalDeRegistro")
	if err != nil {
		return err
	}

	return err
}

var PosicoesRegistroFinalizador = map[string]gerador_layouts_posicoes.Posicao{
	"IdentificadorDoTipoDeRegistro3": {0, 1, 0},
	"SequencialDoArquivo":            {1, 6, 0},
	"QuantidadeTotalDeRegistro":      {6, 11, 0},
}
