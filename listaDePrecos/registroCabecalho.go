package listaDePrecos

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type RegistroCabecalho struct {
	IdentificadorDoTipoDeRegistro1 string `json:"IdentificadorDoTipoDeRegistro1"`
	SequencialDoArquivo            int64  `json:"SequencialDoArquivo"`
	IdentificadorDoTipoPre         string `json:"IdentificadorDoTipoPre"`
	CodigoDoFornecedor             string `json:"CodigoDoFornecedor"`
	UnidadeDaFederacao             string `json:"UnidadeDaFederacao"`
	DataDaGeracaoDoArquivoDePreco  string `json:"DataDaGeracaoDoArquivoDePreco"`
}

func (r *RegistroCabecalho) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistroCabecalho

	err = posicaoParaValor.ReturnByType(&r.IdentificadorDoTipoDeRegistro1, "IdentificadorDoTipoDeRegistro1")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.SequencialDoArquivo, "SequencialDoArquivo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.IdentificadorDoTipoPre, "IdentificadorDoTipoPre")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CodigoDoFornecedor, "CodigoDoFornecedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.UnidadeDaFederacao, "UnidadeDaFederacao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.DataDaGeracaoDoArquivoDePreco, "DataDaGeracaoDoArquivoDePreco")
	if err != nil {
		return err
	}

	return err
}

var PosicoesRegistroCabecalho = map[string]gerador_layouts_posicoes.Posicao{
	"IdentificadorDoTipoDeRegistro1": {0, 1, 0},
	"SequencialDoArquivo":            {1, 6, 0},
	"IdentificadorDoTipoPre":         {6, 9, 0},
	"CodigoDoFornecedor":             {9, 18, 0},
	"UnidadeDaFederacao":             {18, 20, 0},
	"DataDaGeracaoDoArquivoDePreco":  {20, 28, 0},
}
