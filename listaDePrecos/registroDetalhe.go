package listaDePrecos

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type RegistroDetalhe struct {
	IdentificadorDoTipoDeRegistro2                                               string  `json:"IdentificadorDoTipoDeRegistro2"`
	SequencialDoArquivo                                                          int64   `json:"SequencialDoArquivo"`
	CodigoDoProduto                                                              string  `json:"CodigoDoProduto"`
	PrecoUnitario                                                                float64 `json:"PrecoUnitario"`
	PercentualDeDesconto                                                         float64 `json:"PercentualDeDesconto"`
	PercentualDeRepasseDeIcms                                                    float64 `json:"PercentualDeRepasseDeIcms"`
	PercentualDeIcms                                                             float64 `json:"PercentualDeIcms"`
	PercentualDeReducaoDaBaseDeIcms                                              float64 `json:"PercentualDeReducaoDaBaseDeIcms"`
	PrecoBaseParaCalculoDoIcmsPorSubstituicaoTributaria                          float64 `json:"PrecoBaseParaCalculoDoIcmsPorSubstituicaoTributaria"`
	ValorDoIcmsPorSubstituicaoTributaria                                         float64 `json:"ValorDoIcmsPorSubstituicaoTributaria"`
	NumeroDeDiasParaOPagamento                                                   int64   `json:"NumeroDeDiasParaOPagamento"`
	CodigoEanDOProduto                                                           string  `json:"CodigoEanDOProduto"`
	ProdutoHpc                                                                   string  `json:"ProdutoHpc"`
	PercentualDeDescontoAdicionalDoPedidoEnviadoViaEntirePharmalink              float64 `json:"PercentualDeDescontoAdicionalDoPedidoEnviadoViaEntirePharmalink"`
	ValorDoIcmsPorSubstituicaoTributariaParaOsProdutosDaFarmacaiaPopularDoBrasil float64 `json:"ValorDoIcmsPorSubstituicaoTributariaParaOsProdutosDaFarmacaiaPopularDoBrasil"`
	QuantidadeDeEstoqueDisponivel                                                int64   `json:"QuantidadeDeEstoqueDisponivel"`
	PercentualDeMidia                                                            float64 `json:"PercentualDeMidia"`
}

func (r *RegistroDetalhe) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistroDetalhe

	err = posicaoParaValor.ReturnByType(&r.IdentificadorDoTipoDeRegistro2, "IdentificadorDoTipoDeRegistro2")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.SequencialDoArquivo, "SequencialDoArquivo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CodigoDoProduto, "CodigoDoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.PrecoUnitario, "PrecoUnitario")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.PercentualDeDesconto, "PercentualDeDesconto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.PercentualDeRepasseDeIcms, "PercentualDeRepasseDeIcms")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.PercentualDeIcms, "PercentualDeIcms")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.PercentualDeReducaoDaBaseDeIcms, "PercentualDeReducaoDaBaseDeIcms")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.PrecoBaseParaCalculoDoIcmsPorSubstituicaoTributaria, "PrecoBaseParaCalculoDoIcmsPorSubstituicaoTributaria")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ValorDoIcmsPorSubstituicaoTributaria, "ValorDoIcmsPorSubstituicaoTributaria")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.NumeroDeDiasParaOPagamento, "NumeroDeDiasParaOPagamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CodigoEanDOProduto, "CodigoEanDOProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ProdutoHpc, "ProdutoHpc")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.PercentualDeDescontoAdicionalDoPedidoEnviadoViaEntirePharmalink, "PercentualDeDescontoAdicionalDoPedidoEnviadoViaEntirePharmalink")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ValorDoIcmsPorSubstituicaoTributariaParaOsProdutosDaFarmacaiaPopularDoBrasil, "ValorDoIcmsPorSubstituicaoTributariaParaOsProdutosDaFarmacaiaPopularDoBrasil")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.QuantidadeDeEstoqueDisponivel, "QuantidadeDeEstoqueDisponivel")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.PercentualDeMidia, "PercentualDeMidia")
	if err != nil {
		return err
	}

	return err
}

var PosicoesRegistroDetalhe = map[string]gerador_layouts_posicoes.Posicao{
	"IdentificadorDoTipoDeRegistro2":                      {0, 1, 0},
	"SequencialDoArquivo":                                 {1, 6, 0},
	"CodigoDoProduto":                                     {6, 14, 0},
	"PrecoUnitario":                                       {14, 23, 2},
	"PercentualDeDesconto":                                {23, 28, 2},
	"PercentualDeRepasseDeIcms":                           {28, 33, 2},
	"PercentualDeIcms":                                    {33, 38, 2},
	"PercentualDeReducaoDaBaseDeIcms":                     {38, 43, 2},
	"PrecoBaseParaCalculoDoIcmsPorSubstituicaoTributaria": {43, 52, 2},
	"ValorDoIcmsPorSubstituicaoTributaria":                {52, 61, 2},
	"NumeroDeDiasParaOPagamento":                          {61, 64, 0},
	"CodigoEanDOProduto":                                  {64, 84, 0},
	"ProdutoHpc":                                          {84, 85, 0},
	"PercentualDeDescontoAdicionalDoPedidoEnviadoViaEntirePharmalink":              {85, 90, 2},
	"ValorDoIcmsPorSubstituicaoTributariaParaOsProdutosDaFarmacaiaPopularDoBrasil": {90, 99, 2},
	"QuantidadeDeEstoqueDisponivel":                                                {99, 105, 0},
	"PercentualDeMidia":                                                            {105, 110, 2},
}
