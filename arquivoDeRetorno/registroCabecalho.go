package arquivoDeRetorno

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type RegistroCabecalho struct {
	IdentificadorTIpoDeRegistro1           string `json:"IdentificadorTIpoDeRegistro1"`
	SequencialDoArquivo                    int64  `json:"SequencialDoArquivo"`
	IdentificadorDoTipoDeArquivoRet        string `json:"IdentificadorDoTipoDeArquivoRet"`
	CodigoDoFornecedor                     string `json:"CodigoDoFornecedor"`
	CodigoDoCliente                        string `json:"CodigoDoCliente"`
	NumeroDoPedido                         int64  `json:"NumeroDoPedido"`
	IdentificadorDeRejeicaoDoPedido        string `json:"IdentificadorDeRejeicaoDoPedido"`
	DescricaoDoMotivoDaRejeicaoDoPedido    string `json:"DescricaoDoMotivoDaRejeicaoDoPedido"`
	NumeroDoCgcDoCliente                   string `json:"NumeroDoCgcDoCliente"`
	TipoPedido                             string `json:"TipoPedido"`
	HorarioDeCorteParaOFaturamentoDoPedido string `json:"HorarioDeCorteParaOFaturamentoDoPedido"`
}

func (r *RegistroCabecalho) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistroCabecalho

	err = posicaoParaValor.ReturnByType(&r.IdentificadorTIpoDeRegistro1, "IdentificadorTIpoDeRegistro1")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.SequencialDoArquivo, "SequencialDoArquivo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.IdentificadorDoTipoDeArquivoRet, "IdentificadorDoTipoDeArquivoRet")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CodigoDoFornecedor, "CodigoDoFornecedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CodigoDoCliente, "CodigoDoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.NumeroDoPedido, "NumeroDoPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.IdentificadorDeRejeicaoDoPedido, "IdentificadorDeRejeicaoDoPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.DescricaoDoMotivoDaRejeicaoDoPedido, "DescricaoDoMotivoDaRejeicaoDoPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.NumeroDoCgcDoCliente, "NumeroDoCgcDoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.TipoPedido, "TipoPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.HorarioDeCorteParaOFaturamentoDoPedido, "HorarioDeCorteParaOFaturamentoDoPedido")
	if err != nil {
		return err
	}

	return err
}

var PosicoesRegistroCabecalho = map[string]gerador_layouts_posicoes.Posicao{
	"IdentificadorTIpoDeRegistro1":           {0, 1, 0},
	"SequencialDoArquivo":                    {1, 6, 0},
	"IdentificadorDoTipoDeArquivoRet":        {6, 9, 0},
	"CodigoDoFornecedor":                     {9, 18, 0},
	"CodigoDoCliente":                        {18, 24, 0},
	"NumeroDoPedido":                         {24, 34, 0},
	"IdentificadorDeRejeicaoDoPedido":        {34, 35, 0},
	"DescricaoDoMotivoDaRejeicaoDoPedido":    {35, 65, 0},
	"NumeroDoCgcDoCliente":                   {65, 79, 0},
	"TipoPedido":                             {79, 80, 0},
	"HorarioDeCorteParaOFaturamentoDoPedido": {80, 84, 0},
}
