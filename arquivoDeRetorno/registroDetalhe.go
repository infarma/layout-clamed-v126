package arquivoDeRetorno

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type RegistroDetalhe struct {
	IdentificadorTIpoDeRegistro2 string `json:"IdentificadorTIpoDeRegistro2"`
	SequencialDoArquivo          int64  `json:"SequencialDoArquivo"`
	codigoDoProduto              string `json:"codigoDoProduto"`
	QuantidadeAtendida           int64  `json:"QuantidadeAtendida"`
	DescricaoDoMotivoDaRejeicao  string `json:"DescricaoDoMotivoDaRejeicao"`
	CodigoEanDoProduto           string `json:"CodigoEanDoProduto"`
	OrigemDaMercadoria           string `json:"OrigemDaMercadoria"`
}

func (r *RegistroDetalhe) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistroDetalhe

	err = posicaoParaValor.ReturnByType(&r.IdentificadorTIpoDeRegistro2, "IdentificadorTIpoDeRegistro2")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.SequencialDoArquivo, "SequencialDoArquivo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.codigoDoProduto, "codigoDoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.QuantidadeAtendida, "QuantidadeAtendida")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.DescricaoDoMotivoDaRejeicao, "DescricaoDoMotivoDaRejeicao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CodigoEanDoProduto, "CodigoEanDoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.OrigemDaMercadoria, "OrigemDaMercadoria")
	if err != nil {
		return err
	}

	return err
}

var PosicoesRegistroDetalhe = map[string]gerador_layouts_posicoes.Posicao{
	"IdentificadorTIpoDeRegistro2": {0, 1, 0},
	"SequencialDoArquivo":          {1, 6, 0},
	"codigoDoProduto":              {6, 14, 0},
	"QuantidadeAtendida":           {14, 19, 0},
	"DescricaoDoMotivoDaRejeicao":  {19, 69, 0},
	"CodigoEanDoProduto":           {69, 82, 0},
	"OrigemDaMercadoria":           {82, 83, 0},
}
