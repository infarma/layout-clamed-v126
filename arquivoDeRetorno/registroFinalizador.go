package arquivoDeRetorno

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type RegistroFinalizador struct {
	IdentificadorDoRegistro3          string `json:"IdentificadorDoRegistro3"`
	SequencialDoArquivo               int64  `json:"SequencialDoArquivo"`
	NumeroTotalDeProdutosAtendidos    int64  `json:"NumeroTotalDeProdutosAtendidos"`
	NumeroTotaldeProdutosNaoAtendidos int64  `json:"NumeroTotaldeProdutosNaoAtendidos"`
}

func (r *RegistroFinalizador) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistroFinalizador

	err = posicaoParaValor.ReturnByType(&r.IdentificadorDoRegistro3, "IdentificadorDoRegistro3")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.SequencialDoArquivo, "SequencialDoArquivo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.NumeroTotalDeProdutosAtendidos, "NumeroTotalDeProdutosAtendidos")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.NumeroTotaldeProdutosNaoAtendidos, "NumeroTotaldeProdutosNaoAtendidos")
	if err != nil {
		return err
	}

	return err
}

var PosicoesRegistroFinalizador = map[string]gerador_layouts_posicoes.Posicao{
	"IdentificadorDoRegistro3":          {0, 1, 0},
	"SequencialDoArquivo":               {1, 6, 0},
	"NumeroTotalDeProdutosAtendidos":    {6, 11, 0},
	"NumeroTotaldeProdutosNaoAtendidos": {11, 16, 0},
}
