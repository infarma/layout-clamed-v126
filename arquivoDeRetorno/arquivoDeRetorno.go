package arquivoDeRetorno

import (
	"bufio"
	"os"
)

type ArquivoDeRetorno struct {
	RegistroCabecalho   RegistroCabecalho   `json:"RegistroCabecalho"`
	RegistroDetalhe     []RegistroDetalhe   `json:"RegistroDetalhe"`
	RegistroFinalizador RegistroFinalizador `json:"RegistroFinalizador"`
}

func GetStruct(fileHandle *os.File) (ArquivoDeRetorno, error) {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoDeRetorno{}
	var err error
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		identificador := string(runes[0:2])

		switch identificador {
		case "1":
			err := arquivo.RegistroCabecalho.ComposeStruct(fileScanner.Text())
			return arquivo, err
		case "2":
			var registroTemp RegistroDetalhe
			err = registroTemp.ComposeStruct(string(runes))
			arquivo.RegistroDetalhe = append(arquivo.RegistroDetalhe, registroTemp)
			return arquivo, err
		case "3":
			err := arquivo.RegistroFinalizador.ComposeStruct(fileScanner.Text())
			return arquivo, err
		}
	}
	return arquivo, err
}
