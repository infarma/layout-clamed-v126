package layout_clamed_V126

import (
	"bitbucket.org/infarma/layout-clamed-V126/pedido"
	"os"
)

func GetArquivoDePedido(fileHandle *os.File) (pedido.ArquivoDePedido, error) {
	return pedido.GetStruct(fileHandle)
}
